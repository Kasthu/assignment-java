package Part2;

import java.util.Scanner;

/*prime number is a positive integer which is divisible only by 1 and itself for example:2,3,5,7,11 */
public class PrimeNumber {
	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a number");
		// it scans the next token as an int value
		int num = scan.nextInt();
		int i;
		// 2 to num
		for (i = 2; i < num; i++)
			// checks whether num is prime or not.
			if (num % i == 0)
				// if (num%i=0) this loop exit
				break;
		// it's loop is end
		if (i == num)
			System.out.println("Number is prime");
		else
			System.out.println("Number is not prime");
	}
}
